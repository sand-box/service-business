package com.sap.service.cloud.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;

import com.sap.service.cloud.factory.CloudConfiguration;
import com.sap.service.cloud.factory.CloudUtil;
import com.sap.service.contract.ServiceContract;
import com.sap.service.model.Service;

public class ServiceImpl implements ServiceContract {

	public ServiceImpl() {
		if (CloudConfiguration.getInstance().getUrl() == null) {
			CloudConfiguration.getInstance().setUrl(CloudUtil.ENDPOINT);
		}
	}

	public String echo2(String echo) {

		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract utilService = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return utilService.echo(echo);
	}

	@Override
	public Long create(Service service) {
		// insert values in hashmap
		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				// Already encripted
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract serviceContract = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return serviceContract.create(service);
	}

	@Override
	public Service read(long id) {
		// insert values in hashmap
		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				// Already encripted
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract serviceContract = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return serviceContract.read(id);
	}

	@Override
	public List<Service> readAll() {
		// insert values in hashmap
		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				// Already encripted
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract serviceContract = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return serviceContract.readAll();
	}

	@Override
	public Long update(Service service) {

		// insert values in hashmap
		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				// Already encripted
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract serviceContract = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return serviceContract.update(service);
	}

	@Override
	public Service delete(long id) {
		// insert values in hashmap
		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				// Already encripted
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract serviceContract = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return serviceContract.delete(id);
	}

	@Override
	public String echo(String echo) {

		// insert values in hashmap
		Map<String, Object> requestAttribute = new HashMap<String, Object>();
		HttpClient httpClient = new DefaultHttpClient();

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor(httpClient) {
			@Override
			public ClientResponse execute(ClientRequest request) throws Exception {
				// Already encripted
				request.header("Authorization", "Basic YWRtaW46c2VjcmV0");
				return super.execute(request);
			}
		};

		ServiceContract serviceContract = ProxyFactory.create(ServiceContract.class, CloudConfiguration.getInstance().getUrl(), executor);
		return serviceContract.echo(echo);
	}

}
