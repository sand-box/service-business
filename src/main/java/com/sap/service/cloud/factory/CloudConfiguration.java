package com.sap.service.cloud.factory;

import java.util.logging.Handler;

import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

public class CloudConfiguration {

	private static final CloudConfiguration CLOUD_CONFIGURATION = new CloudConfiguration();

	private String url;

	public static CloudConfiguration getInstance() {
		return CloudConfiguration.CLOUD_CONFIGURATION;
	}

	/**
	 * 
	 */
	private CloudConfiguration() {
		java.util.logging.Logger rootLogger = java.util.logging.Logger.getLogger("");
		Handler[] handlers = rootLogger.getHandlers();
		if (handlers != null && handlers.length > 0) {
			rootLogger.removeHandler(handlers[0]);
		}
		RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
