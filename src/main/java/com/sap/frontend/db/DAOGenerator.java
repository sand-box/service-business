package com.sap.frontend.db;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DAOGenerator {
	private static final String OUTPUT_FOLDER = "src/main/java/";
	
	public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.sap.frontedn.model");
        addService(schema);
       
        new DaoGenerator().generateAll(schema, OUTPUT_FOLDER);
    }
    private static void addService(Schema schema) {
        Entity service = schema.addEntity("Service");
        service.addIdProperty().autoincrement();
        service.addStringProperty("name");
        service.addStringProperty("description");
        service.addStringProperty("note");
        service.addDoubleProperty("estimatedDuration");
        service.addDoubleProperty("setFee");
        service.addDoubleProperty("totalCost");
    
    }
   
}
